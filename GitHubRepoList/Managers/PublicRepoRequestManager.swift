//
//  PublicRepoRequestManager.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 30.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import UIKit

struct PublicRepoRequestManager: NetworkManagerProtocol {
    
    func getPublicRepo(atIndex: Int, success: @escaping RepositoriesResultBlock, failure: @escaping ErrorStringResultBlock) {
        self.sendGetRequest(Constants.URLs.getPublicRepo, parameters: ["since": "\(atIndex)"]) { (response, error) in
            if let error = error {
                return failure(error.localizedDescription)
            }
            guard let repos = response as? [[String : Any]] else {
                return failure("Get Repositories Error")
            }
            var repositories = [RepositoryModel]()
            for repository in repos {
                guard let repo = RepositoryModel(JSON: repository) else {
                    return failure("Get Object Repository Error")
                }
                repositories.append(repo)
            }
            success(repositories)
        }
    }
    
    func getRepoSubs(_ repo: RepositoryModel, success: @escaping WatchersResultBlock, failure: @escaping ErrorStringResultBlock) {
        self.sendGetRequest(Constants.URLs.getSubs(repo), parameters: nil) { (response, error) in
            if let error = error {
                return failure(error.localizedDescription)
            }
            guard let watchers = response as? [[String : Any]] else {
                return failure("Get Watchers Error")
            }
            var users = [UserModel]()
            for watcher in watchers {
                guard let user = UserModel(JSON: watcher) else {
                    return failure("Get Object Watcher Error")
                }
                users.append(user)
            }
            success(users)
        }
    }
}
