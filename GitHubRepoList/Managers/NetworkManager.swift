//
//  NetworkManager.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 29.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkManagerProtocol {
    func sendGetRequest(_ url: String, parameters: [String : Any]?, result: @escaping NetworkResultBlock)
}

extension NetworkManagerProtocol {
    func sendGetRequest(_ url: String, parameters: [String : Any]?, result: @escaping NetworkResultBlock) {
        UIApplication.showHUD()
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { (response) in
            UIApplication.hideHUD()
            if let error = response.error {
                result([], error)
                return
            }
            if let json = response.result.value {
                result(json, nil)
            } else {
                UIApplication.showAlertMessageWithAction(message: "Response JSON value Error", handler: { (_) in
                    result([], nil)
                })
            }
        }
    }
}
