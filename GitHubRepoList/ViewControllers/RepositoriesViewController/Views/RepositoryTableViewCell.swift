//
//  RepositoryTableViewCell.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 18.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var ownerImage: UIImageView!
    
    
    func cellSource(_ repo: RepositoryModel) {
        nameLabel.text = repo.name.uppercased()
        descriptionLabel.text = repo.descr
        forksLabel.text = "\(repo.forks)"
        ownerImage.sd_setImage(with: URL(string: repo.owner.avatarUrl),
                               placeholderImage: UIImage(named: "defaultUserLogo"))
    }
    
}
