//
//  RepositoriesViewController.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 29.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let detailSegueIdentifier = "RepositoryInfoController"
    fileprivate var repositories = [RepositoryModel]()
    fileprivate var watchers     = [UserModel]()

    fileprivate var loadRepoOffset: Int {
        return repositories.count - 10
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Public Repositories"
        
        tableView.registerCell(RepositoryTableViewCell.self)
        getNewRepo(0)
    }
    
    //MARK: - Private
    private func getNewRepo(_ index: Int) {
        PublicRepoRequestManager().getPublicRepo(atIndex: index, success: { (repos) in
            self.repositories += repos
            self.tableView.reloadData()
        }) { (errorMessage) in
            UIApplication.showAlertMessage(message: errorMessage)
        }
    }
    
    private func getSubs(_ indexPath: IndexPath) {
        PublicRepoRequestManager().getRepoSubs(repositories[indexPath.row], success: { (watchers) in
            self.watchers = watchers
            self.performSegue(withIdentifier: self.detailSegueIdentifier, sender: nil)
        }) { (errorMessage) in
            UIApplication.showAlertMessage(message: errorMessage)
        }
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == detailSegueIdentifier,
            let controller = segue.destination as? RepositoryDetailViewController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            controller.repo  = repositories[indexPath.row]
            controller.users = watchers
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }

}
//MARK: - UITableViewDataSource
extension RepositoriesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(cls: RepositoryTableViewCell.self, indexPath: indexPath) else {
            return UITableViewCell()
        }
        cell.cellSource(repositories[indexPath.row])
        if indexPath.row == loadRepoOffset {
            getNewRepo(repositories.endIndex)
        }
        return cell
    }
}

//MARK: - UITableViewDelegate
extension RepositoriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getSubs(indexPath)
    }
}

//MARK: - UIScrollViewDelegate
extension RepositoriesViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            getNewRepo(repositories.endIndex)
        }
    }
}



