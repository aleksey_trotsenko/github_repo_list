//
//  RepositoryDetailViewController.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 01.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class RepositoryDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countWatchersLabel: UILabel!
    
    var users: [UserModel]?
    var repo:  RepositoryModel?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let repo = repo, let users = users else {
            UIApplication.showAlertMessageWithAction(message: "Subs not found", handler: { (_) in
                self.navigationController?.popViewController(animated: true)
            })
            return
        }
        title = repo.name.uppercased()
        countWatchersLabel.text = "\(users.count)"
        self.tableView.registerCell(UserTableViewCell.self)
    }
}


//MARK: - TableViewDataSource
extension RepositoryDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(cls: UserTableViewCell.self, indexPath: indexPath) else {
            return UITableViewCell()
        }
        cell.cellSource(users![indexPath.row])
        return cell
    }
}
