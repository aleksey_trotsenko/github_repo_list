//
//  UserTableViewCell.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 18.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import SDWebImage

class UserTableViewCell: UITableViewCell {
    
    func cellSource(_ user: UserModel) {
        self.imageView?.sd_setImage(with: URL(string: user.avatarUrl),
                                    placeholderImage: UIImage(named: "defaultUserLogo"))
        self.textLabel?.text = user.login
    }
}
