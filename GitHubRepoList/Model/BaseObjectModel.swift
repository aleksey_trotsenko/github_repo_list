//  BaseObjectModel.swift
//  Spot
//
//  Created by Kortez on 01.03.17.

import Foundation
import ObjectMapper

class BaseObjectModel : NSObject, Mappable{

    required override init() {
        super.init()
    }
    
    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {}
}
