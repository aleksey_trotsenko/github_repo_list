//
//  OwnerModel.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 30.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModel: BaseObjectModel {
    
    var avatarUrl = ""
    var login     = ""
    
    override func mapping(map: Map) {
        avatarUrl <- map["avatar_url"]
        login     <- map["login"]
    }
}

