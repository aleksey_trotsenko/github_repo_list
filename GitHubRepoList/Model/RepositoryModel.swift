//
//  RepositoryModel.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 30.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryModel: BaseObjectModel {
    
    var name   = ""
    var descr  = ""
    var forks  = 0
    var owner  = UserModel()
    
    override func mapping(map: Map) {
        name  <- map["name"]
        descr <- map["description"]
        forks <- map["fork"]
        owner <- map["owner"]
    }
}

