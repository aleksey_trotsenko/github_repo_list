//  RootRouter.swift
//  Spot

import UIKit
import SVProgressHUD

class RootRouter {
    
    var topViewController: UIViewController? {
        return UIApplication.topViewController()
    }
    
    /// window for navigation
    var window: UIWindow?
    
    // MARK: - Singleton
    static let shared = RootRouter()
    
    /// Custom didFinishLaunchingWithOptions method
    /// Tells the delegate that the launch process is almost done and the app is almost ready to run.
    ///
    /// - Parameters:
    ///   - launchOptions: A dictionary indicating the reason the app was launched
    ///   - window: The app's visible window
    /// - Returns: NO if the app cannot handle the URL resource or continue a user activity, otherwise return YES. The return value is ignored if the app is launched as a result of a remote notification.
    
    func application(didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?, window: UIWindow) -> Bool {
        RootRouter.shared.window = window
        return true
    }
}

// MARK: - Additional extension for UIApplication
extension UIApplication {
    
    //MARK: Activity indicator
    class func showHUD() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
    }
    
    class func hideHUD(){
        SVProgressHUD.dismiss()
    }
    
    //MARK: Get top ViewController
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = base as? UINavigationController {
            return topViewController(navigationController.visibleViewController)
        }
        
        if let tabBarController = base as? UITabBarController {
            if let selected = tabBarController.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presentedViewController = base?.presentedViewController {
            return topViewController(presentedViewController)
        }
        
        if base == nil {
            return UIApplication.shared.delegate?.window??.rootViewController
        }
        return base
    }
    
    
    //MARK: - AlertView
    class func showAlertMessage(message text: String){
        let alert = UIAlertController(title: "Information", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        
        DispatchQueue.main.async(execute: {
            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
        })
    }
    
    class func showAlertMessageWithAction(message text: String, handler: @escaping (UIAlertAction) -> Void){
        
        let alert = UIAlertController(title: "Information", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: handler))
        
        DispatchQueue.main.async(execute: {
            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
        })
    }
}
