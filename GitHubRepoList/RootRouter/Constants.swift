//
//  Constants.swift
//  GitHubRepoList
//
//  Created by Alexey Trotsenko on 29.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation

//MARK: - RequestURLs
enum Constants {
    enum URLs {
        static let baseUrl = "https://api.github.com"
        static let getPublicRepo = "\(baseUrl)/repositories"
        static func getSubs(_ repo: RepositoryModel) -> String {
            return "\(baseUrl)/repos/\(repo.owner.login)/\(repo.name)/subscribers"
        }
    }
}


typealias ErrorStringResultBlock  = (_ errorString: String) -> ()
typealias NetworkResultBlock      = (_ response: Any, Error?)->()

typealias RepositoriesResultBlock = ([RepositoryModel]) -> ()
typealias WatchersResultBlock     = ([UserModel]) -> ()

